<!DOCTYPE html>
<html lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Datengin! | </title>

    <!-- Bootstrap -->
    <link href="{{asset('gentelella/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{asset('gentelella/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <!-- Animate.css -->
    <link href="https://colorlib.com/polygon/gentelella/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="{{asset('gentelella/build/css/custom.min.css')}}" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">

              {!! Form::open(['action' => ['Auth\LoginController@login']]) !!}
              <h1>Login Form</h1>

              <div class="form-group">
                {!! Form::text('email',null,["class" =>"form-control", "placeholder"=>"Email"]) !!}
                <!-- <input type="text" class="form-control" placeholder="Username" required="" /> -->

              </div>
              <div class="form-group">
                {!! Form::password('password',["class" =>"form-control", "placeholder"=>"Password"]) !!}
                <!-- <input type="password" class="form-control" placeholder="Password" required="" /> -->

              </div>

              <div>
                {!! Form::submit('Log in', ["class"=>"btn btn-default submit"]) !!}
                <a class="reset_pass" href="#">Lost your password?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">

                <!-- <p class="change_link">New to site?


                  <a   href="" class="to_register">  Create Account </a>
                </p> -->

                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-paw"></i> Datengin!</h1>
                  <p>©2016 All Rights Reserved. Datengin! Privacy and Terms</p>
                </div>
              </div>
            {!! Form::close()!!}
          </section>
        </div>


      </div>
    </div>
  </body>
</html>
