@extends('admin.layouts.blank')
@section('title','Show')
@section('content')

<h3>Nama :{{$data->name}}</h3>

<hr>
<a href="{{action('Admin\CategoryController@index')}}" class="btn btn-info"> Back to all tasks</a>
<a href="{{action('Admin\CategoryController@edit', $data->id)}}" class="btn btn-primary"> Edit</a>
<a href="{{action('Admin\CategoryController@delete', $data->id)}}" class="btn btn-danger"> Delete</a>

  @endsection
