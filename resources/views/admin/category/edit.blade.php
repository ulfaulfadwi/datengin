@extends('admin.layouts.blank')
@section('title','Edit')
@section('content')
  {!! Form::model($data,['method'=>'put','action'=>['Admin\CategoryController@update',$data->id ]])!!}
<div class="form-group">
    {!! Form::label('name','name:',['class'=>'control-label'])!!}
    {!! Form::text('name',null,['class'=>'form-control'])!!}
  </div>

  {!! Form::submit('Update Category', ['class' => 'btn btn-primary']) !!}
  {!! Form::close()!!}

  @endsection
