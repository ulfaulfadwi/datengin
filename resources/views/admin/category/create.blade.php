@extends('admin.layouts.blank')
@section('title','Create')
@section('content')

<h1>Add a New Category</h1>
<hr>

{!! Form::open(['action' => ['Admin\CategoryController@store']]) !!}

<div class="form-group">
        {!! Form::label('name', 'name:', ['class' => 'control-label']) !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>


{!! Form::submit('Create New Category', ['class' => 'btn btn-primary']) !!}

{!! Form::close() !!}

@endsection
