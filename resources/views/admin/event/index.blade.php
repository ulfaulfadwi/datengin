@extends('admin.layouts.blank')

@section('title','Index')

@section('content')


<h1>Event List</h1>
        <p class="lead" ></p>
        <div class="table-responsive">
            <table class="table table-bordered">
              <tr>
                <th>No.</th>
                <th>Title</th>
                <th>Description</th>
                <th>Price</th>
                <th>Date</th>
                <th>Location</th>
                <th>Speaker</th>
                <th>Img_url</th>
                <th>Options</th>

              </tr>

        @foreach($data as $event)

        <tr>
          <td>
            {{ $loop->iteration}}
          </td>
        <td>{{$event->title}}</td>
        <td>{{$event->description}}</td>
        <td>{{$event->price}}</td>
        <td>{{$event->date}}</td>
        <td>{{$event->location}}</td>
        <td>{{$event->speaker}}</td>
        <td>{{$event->img_url}}</td>
        <td>{{$event->options}}
                <a href="{{action('Admin\EventController@show',$event->id )}}" class="btn btn-info"> View Event</a>
                <a href="{{action('Admin\EventController@edit', $event->id)}}" class="btn btn-primary"> Edit Event</a>
                <a href="{{action('Admin\EventController@delete', $event->id)}}" class="btn btn-danger"> Delete Event</a>
              </td>
      </tr>
        <hr>
      @endforeach
    </table>
  </div>



@endsection
