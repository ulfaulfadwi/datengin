@extends('admin.layouts.blank')
@section('title','Show')
@section('content')

<h3>Title :{{$data->title}}</h3>
<h3>Description :{{$data->description}}</h3>
<h3>Price :{{$data->price}}</h3>
<h3>Date :{{$data->date}}</h3>
<h3>Location :{{$data->location}}</h3>
<h3>Speaker :{{$data->speaker}}</h3>
<h3>Img_url : <img src="{{asset($data->img_url)}}" alt=""></h3>

<hr>
<a href="{{action('Admin\EventController@index')}}" class="btn btn-info"> Back to all lists</a>
<a href="{{action('Admin\EventController@edit', $data->id)}}" class="btn btn-primary"> Edit Event</a>
<a href="{{action('Admin\EventController@delete', $data->id)}}" class="btn btn-danger"> Delete Event</a>

  @endsection
