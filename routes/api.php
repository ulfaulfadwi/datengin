<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
// Route::resource('/home', 'HomeController');

Route::group(['middleware'=>['jwt.auth']],function(){
    Route::resource('/user','Api\UserController');
});
// Route::post('/login','Api\AuthController@login');
Route::get('/log/admin','Api\AuthController@getLoginUser');
Route::post('/regist','Api\AuthController@register');
Route::get('event/liked', 'Api\EventController@likedEvents');
Route::post('event/{id}/like', 'Api\EventController@like');
Route::post('event/{id}/unlike', 'Api\EventController@unlike');
Route::resource('event', 'Api\EventController',['only' => ['index', 'show']]);
Route::resource('category', 'Api\CategoryController',['only' => ['index']]);
Route::resource('comment', 'Api\CommentController');
Route::post('category/{id}/favorite', 'Api\CategoryController@favorite');
Route::post('category/{id}/unfavorite', 'Api\CategoryController@unfavorite');
Route::post('like/{id}', 'ExController@like');
Route::get('/showuser','ExController@showevent');
Route::post('/image','ExController@image');
