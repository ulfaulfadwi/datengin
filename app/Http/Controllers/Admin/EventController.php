<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use App\Event;
use App\Category;
use Image;
use Auth;
class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Event::all();
        return view('admin.event.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories=Category::pluck('name', 'id')->all();
        return view('admin.event.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $input=$request->all();
      $input['user_id']=Auth::user()->id;
      $image=$request->img;
      $event=Event::create($input);
      if(!is_dir('images/event')){
        mkdir('images/event', 755, true);
      }
      $img_url='images/event/'.$event->id.'.'.$image->getClientOriginalExtension();
      Image::make($image)->resize(300, 200)->save($img_url);
      $event->update(['img_url'=>$img_url]);
      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data= Event::find($id);
      return view('admin.event.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data= Event::find($id);
      return view('admin.event.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      Event::find($id)->update($request->all());
      return redirect()->action('Admin\EventController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Event::destroy($id);
      return redirect()->action('Admin\EventController@index');
    }
    public function delete($id)
  {
      Event::destroy($id);
      return redirect()->action('Admin\EventController@index');
  }
}
