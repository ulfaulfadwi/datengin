<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\User;
use App\Like;
use JWTAuth;
class ExController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $response=Event::where('user_id','=',$id)->get();
      return response()->json($response);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function like(Request $request,$id)
    {
        $user=JWTAuth::parseToken()->authenticate();
        $input=$request->all();
        $input['user_id']=$user->id;
        $input['event_id']=$id;
        $data=Like::where('event_id','=',$id)->where('user_id',"=",$user->id)->get()->count();
        if (0==$data) {
          Like::create($input);
          return response()->json($request);
        }
        $error['status']="failed";
        return response()->json($error);
    }
    public function showlike($id)
    {
      $data['like']=Like::where('event_id','=',$id)->get()->count();
      return response()->json($data);
    }
    public function showevent()
    {
      $user=JWTAuth::parseToken()->authenticate();
      return response()->json($user);
    }
    public function image(Request $request)
    {
      $user=JWTAuth::parseToken()->authenticate();
      return response()->json($request->all());
    }
}
